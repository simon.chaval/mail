﻿namespace Mail_Final
{
    partial class ModifierPJ
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstPiecesJointes = new System.Windows.Forms.ListBox();
            this.lblPJ = new System.Windows.Forms.Label();
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstPiecesJointes
            // 
            this.lstPiecesJointes.FormattingEnabled = true;
            this.lstPiecesJointes.Location = new System.Drawing.Point(45, 97);
            this.lstPiecesJointes.Name = "lstPiecesJointes";
            this.lstPiecesJointes.Size = new System.Drawing.Size(249, 212);
            this.lstPiecesJointes.TabIndex = 0;
            // 
            // lblPJ
            // 
            this.lblPJ.AutoSize = true;
            this.lblPJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPJ.Location = new System.Drawing.Point(125, 51);
            this.lblPJ.Name = "lblPJ";
            this.lblPJ.Size = new System.Drawing.Size(89, 13);
            this.lblPJ.TabIndex = 1;
            this.lblPJ.Text = "Pièces Jointes";
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.Location = new System.Drawing.Point(128, 343);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(75, 23);
            this.btnSupprimer.TabIndex = 2;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.UseVisualStyleBackColor = true;
            this.btnSupprimer.Click += new System.EventHandler(this.btnSupprimer_Click);
            // 
            // ModifierPJ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(346, 396);
            this.Controls.Add(this.btnSupprimer);
            this.Controls.Add(this.lblPJ);
            this.Controls.Add(this.lstPiecesJointes);
            this.Name = "ModifierPJ";
            this.Text = "Modifier Pièces Jointes";
            this.Load += new System.EventHandler(this.ModifierPJ_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstPiecesJointes;
        private System.Windows.Forms.Label lblPJ;
        private System.Windows.Forms.Button btnSupprimer;
    }
}