﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mail_Final
{
    static class Outil
    {
        //
        // Résumé :
        //    Ecrit dans le fichier texte Annuaire les différents champs remplis pour l'ajout d'un contact
        //
        // Retourne :
        //    Liste de personne  
        //
        public static List<Personne> lectureContact()
        {
            List<Personne> contacts = new List<Personne>();

            try
            {
                string[] lignes = File.ReadAllLines(@"..\\..\\..\\Annuaire.txt");
                foreach (String ligne in lignes)
                {
                    if (ligne == "")
                    {
                        break;
                    }

                    Personne p = new Personne();
                    string[] elements = ligne.Split(';');
                    p.Nom = elements[0];
                    p.Prenom = elements[1];
                    p.setMail(elements[2]);
                    p.Ville = elements[3];
                    p.CodePostal = elements[4];
                    p.Telephone = elements[5];

                    contacts.Add(p);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impssible de charger le fichier Annuaire.txt", "Message d'erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


            return contacts;
        }
        //
        // Résumé :
        //    Permet d'enregistrer le contact dans mon fichier texte Annuaire
        //
        // Paramètres :
        //    contacts :
        //          Chaine de caractères
        //
        public static void enregistrerContact(List<Personne> contacts)
        {
            StreamWriter monSW = new StreamWriter(@"..\\..\\..\\Annuaire.txt");
            foreach (Personne p in contacts)
            {
                String ligne = p.convertiCsv();
                monSW.WriteLine(ligne);
            }
            monSW.Close();
            monSW.Dispose();
        }

        //
        // Résumé :
        //    Lecture du fichier texte Serveur pour l'envoie du mail
        //       
        // Retourne :
        //    Chaine de caractère présente dans le fichier texte Serveur
        //
        public static List<String> listeServeur()
        {
            return File.ReadAllLines(@"..\\..\\..\\Serveur.txt").ToList();
        }
    }
}
