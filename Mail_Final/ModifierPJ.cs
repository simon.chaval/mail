﻿using System;
using System.Net.Mail;
using System.Windows.Forms;

namespace Mail_Final
{
    public partial class ModifierPJ : Form
    {
        private AttachmentCollection piecejointe;
        public ModifierPJ(AttachmentCollection piecejointe)
        {
            InitializeComponent();
            this.piecejointe = piecejointe;
        }
        //
        // Résumé :
        //    Actualisation de la liste
        //
        private void ModifierPJ_Load(object sender, EventArgs e)
        {
            chargerListes();
        }
        //
        // Résumé :
        //    Permet d'ajouter chaque élément à la liste
        //
        private void chargerListes()
        {
            lstPiecesJointes.Items.Clear();
            foreach (Attachment a in piecejointe)
            {
                lstPiecesJointes.Items.Add(a.Name);
            }
        }
        //
        // Résumé :
        //    Permet de supprimer une pièce jointe ajouter au mail
        //
        private void btnSupprimer_Click(object sender, EventArgs e)
        {
            DialogResult confirmation = MessageBox.Show("Etes-vous sur de vouloir supprimer ?", "Supprimer", MessageBoxButtons.YesNo);

            if (confirmation == DialogResult.Yes)
            {
                foreach (int selection in lstPiecesJointes.SelectedIndices)
                {
                    piecejointe.RemoveAt(selection);
                }
                chargerListes();
            }
        }
    }
}
