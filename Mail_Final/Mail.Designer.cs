﻿namespace Mail_Final
{
    partial class Mail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.tbxSansAnnuaire = new System.Windows.Forms.TextBox();
            this.btnAjouterAvecAnnuaire = new System.Windows.Forms.Button();
            this.btnEnvoiMail = new System.Windows.Forms.Button();
            this.btnAjouterSansAnnuaire = new System.Windows.Forms.Button();
            this.btnAnnuaire = new System.Windows.Forms.Button();
            this.btnParcourir = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.cbxAvecAnnuaire = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbxSansAnnuaire = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnQuitter = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cbxAnnuaire = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbxMessage = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxPiecesJointes = new System.Windows.Forms.TextBox();
            this.cbxServeur = new System.Windows.Forms.ComboBox();
            this.tbxObjet = new System.Windows.Forms.TextBox();
            this.tbxExpediteur = new System.Windows.Forms.TextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnModifierPJ = new System.Windows.Forms.Button();
            this.btnAjouterPJ = new System.Windows.Forms.Button();
            this.btnModifier = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(0, 0);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(550, 593);
            this.webBrowser1.TabIndex = 26;
            // 
            // tbxSansAnnuaire
            // 
            this.tbxSansAnnuaire.Location = new System.Drawing.Point(162, 168);
            this.tbxSansAnnuaire.Name = "tbxSansAnnuaire";
            this.tbxSansAnnuaire.Size = new System.Drawing.Size(166, 20);
            this.tbxSansAnnuaire.TabIndex = 38;
            // 
            // btnAjouterAvecAnnuaire
            // 
            this.btnAjouterAvecAnnuaire.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAjouterAvecAnnuaire.Location = new System.Drawing.Point(490, 199);
            this.btnAjouterAvecAnnuaire.Name = "btnAjouterAvecAnnuaire";
            this.btnAjouterAvecAnnuaire.Size = new System.Drawing.Size(75, 23);
            this.btnAjouterAvecAnnuaire.TabIndex = 52;
            this.btnAjouterAvecAnnuaire.Text = "Ajouter";
            this.btnAjouterAvecAnnuaire.UseVisualStyleBackColor = true;
            this.btnAjouterAvecAnnuaire.Click += new System.EventHandler(this.btnAjouterAvecAnnuaire_Click);
            // 
            // btnEnvoiMail
            // 
            this.btnEnvoiMail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnvoiMail.Location = new System.Drawing.Point(68, 502);
            this.btnEnvoiMail.Name = "btnEnvoiMail";
            this.btnEnvoiMail.Size = new System.Drawing.Size(98, 25);
            this.btnEnvoiMail.TabIndex = 28;
            this.btnEnvoiMail.Text = "Envoyer";
            this.btnEnvoiMail.UseVisualStyleBackColor = true;
            this.btnEnvoiMail.Click += new System.EventHandler(this.btnEnvoiMail_Click);
            // 
            // btnAjouterSansAnnuaire
            // 
            this.btnAjouterSansAnnuaire.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAjouterSansAnnuaire.Location = new System.Drawing.Point(490, 167);
            this.btnAjouterSansAnnuaire.Name = "btnAjouterSansAnnuaire";
            this.btnAjouterSansAnnuaire.Size = new System.Drawing.Size(75, 23);
            this.btnAjouterSansAnnuaire.TabIndex = 51;
            this.btnAjouterSansAnnuaire.Text = "Ajouter";
            this.btnAjouterSansAnnuaire.UseVisualStyleBackColor = true;
            this.btnAjouterSansAnnuaire.Click += new System.EventHandler(this.btnAjouterSansAnnuaire_Click);
            // 
            // btnAnnuaire
            // 
            this.btnAnnuaire.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnnuaire.Location = new System.Drawing.Point(172, 502);
            this.btnAnnuaire.Name = "btnAnnuaire";
            this.btnAnnuaire.Size = new System.Drawing.Size(99, 25);
            this.btnAnnuaire.TabIndex = 29;
            this.btnAnnuaire.Text = "Annuaire";
            this.btnAnnuaire.UseVisualStyleBackColor = true;
            this.btnAnnuaire.Click += new System.EventHandler(this.btnAnnuaire_Click);
            // 
            // btnParcourir
            // 
            this.btnParcourir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnParcourir.Location = new System.Drawing.Point(351, 289);
            this.btnParcourir.Name = "btnParcourir";
            this.btnParcourir.Size = new System.Drawing.Size(104, 23);
            this.btnParcourir.TabIndex = 50;
            this.btnParcourir.Text = "Parcourir";
            this.btnParcourir.UseVisualStyleBackColor = true;
            this.btnParcourir.Click += new System.EventHandler(this.btnParcourir_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(65, 106);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 30;
            this.label7.Text = "Serveur SMTP";
            // 
            // cbxAvecAnnuaire
            // 
            this.cbxAvecAnnuaire.FormattingEnabled = true;
            this.cbxAvecAnnuaire.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbxAvecAnnuaire.Items.AddRange(new object[] {
            "Destinataire",
            "Copie",
            "Copie cachée"});
            this.cbxAvecAnnuaire.Location = new System.Drawing.Point(351, 201);
            this.cbxAvecAnnuaire.Name = "cbxAvecAnnuaire";
            this.cbxAvecAnnuaire.Size = new System.Drawing.Size(104, 21);
            this.cbxAvecAnnuaire.TabIndex = 49;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(65, 172);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 31;
            this.label6.Text = "Ajouter";
            // 
            // cbxSansAnnuaire
            // 
            this.cbxSansAnnuaire.FormattingEnabled = true;
            this.cbxSansAnnuaire.Items.AddRange(new object[] {
            "Destinataire",
            "Copie",
            "Copie cachée"});
            this.cbxSansAnnuaire.Location = new System.Drawing.Point(351, 168);
            this.cbxSansAnnuaire.Name = "cbxSansAnnuaire";
            this.cbxSansAnnuaire.Size = new System.Drawing.Size(104, 21);
            this.cbxSansAnnuaire.TabIndex = 48;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 204);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "Ajouter depuis annuaire";
            // 
            // btnQuitter
            // 
            this.btnQuitter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuitter.Location = new System.Drawing.Point(381, 502);
            this.btnQuitter.Name = "btnQuitter";
            this.btnQuitter.Size = new System.Drawing.Size(100, 25);
            this.btnQuitter.TabIndex = 47;
            this.btnQuitter.Text = "Quitter";
            this.btnQuitter.UseVisualStyleBackColor = true;
            this.btnQuitter.Click += new System.EventHandler(this.btnQuitter_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(65, 234);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 33;
            this.label4.Text = "Expéditeur";
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnnuler.Location = new System.Drawing.Point(277, 502);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(98, 25);
            this.btnAnnuler.TabIndex = 46;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = true;
            this.btnAnnuler.Click += new System.EventHandler(this.btnAnnuler_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(65, 266);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 34;
            this.label3.Text = "Objet";
            // 
            // cbxAnnuaire
            // 
            this.cbxAnnuaire.FormattingEnabled = true;
            this.cbxAnnuaire.Location = new System.Drawing.Point(162, 201);
            this.cbxAnnuaire.Name = "cbxAnnuaire";
            this.cbxAnnuaire.Size = new System.Drawing.Size(166, 21);
            this.cbxAnnuaire.TabIndex = 43;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(64, 294);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 35;
            this.label2.Text = "Pièces Jointes";
            // 
            // tbxMessage
            // 
            this.tbxMessage.Location = new System.Drawing.Point(162, 322);
            this.tbxMessage.Multiline = true;
            this.tbxMessage.Name = "tbxMessage";
            this.tbxMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbxMessage.Size = new System.Drawing.Size(293, 160);
            this.tbxMessage.TabIndex = 42;
            this.tbxMessage.TextChanged += new System.EventHandler(this.tbxMessage_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 325);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 36;
            this.label1.Text = "Message";
            // 
            // tbxPiecesJointes
            // 
            this.tbxPiecesJointes.Location = new System.Drawing.Point(162, 291);
            this.tbxPiecesJointes.Name = "tbxPiecesJointes";
            this.tbxPiecesJointes.Size = new System.Drawing.Size(166, 20);
            this.tbxPiecesJointes.TabIndex = 41;
            // 
            // cbxServeur
            // 
            this.cbxServeur.FormattingEnabled = true;
            this.cbxServeur.Location = new System.Drawing.Point(162, 103);
            this.cbxServeur.Name = "cbxServeur";
            this.cbxServeur.Size = new System.Drawing.Size(166, 21);
            this.cbxServeur.TabIndex = 37;
            // 
            // tbxObjet
            // 
            this.tbxObjet.Location = new System.Drawing.Point(162, 263);
            this.tbxObjet.Name = "tbxObjet";
            this.tbxObjet.Size = new System.Drawing.Size(166, 20);
            this.tbxObjet.TabIndex = 40;
            this.tbxObjet.TextChanged += new System.EventHandler(this.tbxObjet_TextChanged);
            // 
            // tbxExpediteur
            // 
            this.tbxExpediteur.Location = new System.Drawing.Point(162, 231);
            this.tbxExpediteur.Name = "tbxExpediteur";
            this.tbxExpediteur.Size = new System.Drawing.Size(166, 20);
            this.tbxExpediteur.TabIndex = 39;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.statusStrip1);
            this.splitContainer1.Panel1.Controls.Add(this.btnModifierPJ);
            this.splitContainer1.Panel1.Controls.Add(this.btnAjouterPJ);
            this.splitContainer1.Panel1.Controls.Add(this.btnModifier);
            this.splitContainer1.Panel1.Controls.Add(this.tbxSansAnnuaire);
            this.splitContainer1.Panel1.Controls.Add(this.btnAjouterAvecAnnuaire);
            this.splitContainer1.Panel1.Controls.Add(this.btnEnvoiMail);
            this.splitContainer1.Panel1.Controls.Add(this.btnAjouterSansAnnuaire);
            this.splitContainer1.Panel1.Controls.Add(this.btnAnnuaire);
            this.splitContainer1.Panel1.Controls.Add(this.btnParcourir);
            this.splitContainer1.Panel1.Controls.Add(this.label7);
            this.splitContainer1.Panel1.Controls.Add(this.cbxAvecAnnuaire);
            this.splitContainer1.Panel1.Controls.Add(this.label6);
            this.splitContainer1.Panel1.Controls.Add(this.cbxSansAnnuaire);
            this.splitContainer1.Panel1.Controls.Add(this.label5);
            this.splitContainer1.Panel1.Controls.Add(this.btnQuitter);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.btnAnnuler);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.cbxAnnuaire);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.tbxMessage);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.tbxPiecesJointes);
            this.splitContainer1.Panel1.Controls.Add(this.cbxServeur);
            this.splitContainer1.Panel1.Controls.Add(this.tbxObjet);
            this.splitContainer1.Panel1.Controls.Add(this.tbxExpediteur);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.webBrowser1);
            this.splitContainer1.Size = new System.Drawing.Size(1263, 593);
            this.splitContainer1.SplitterDistance = 709;
            this.splitContainer1.TabIndex = 54;
            // 
            // btnModifierPJ
            // 
            this.btnModifierPJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifierPJ.Location = new System.Drawing.Point(520, 289);
            this.btnModifierPJ.Name = "btnModifierPJ";
            this.btnModifierPJ.Size = new System.Drawing.Size(53, 23);
            this.btnModifierPJ.TabIndex = 55;
            this.btnModifierPJ.Text = "Modifier";
            this.btnModifierPJ.UseVisualStyleBackColor = true;
            this.btnModifierPJ.Click += new System.EventHandler(this.btnModifierPJ_Click);
            // 
            // btnAjouterPJ
            // 
            this.btnAjouterPJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAjouterPJ.Location = new System.Drawing.Point(461, 289);
            this.btnAjouterPJ.Name = "btnAjouterPJ";
            this.btnAjouterPJ.Size = new System.Drawing.Size(53, 23);
            this.btnAjouterPJ.TabIndex = 54;
            this.btnAjouterPJ.Text = "Ajouter";
            this.btnAjouterPJ.UseVisualStyleBackColor = true;
            this.btnAjouterPJ.Click += new System.EventHandler(this.btnAjouterPJ_Click);
            // 
            // btnModifier
            // 
            this.btnModifier.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifier.Location = new System.Drawing.Point(571, 182);
            this.btnModifier.Name = "btnModifier";
            this.btnModifier.Size = new System.Drawing.Size(75, 23);
            this.btnModifier.TabIndex = 53;
            this.btnModifier.Text = "Modifier";
            this.btnModifier.UseVisualStyleBackColor = true;
            this.btnModifier.Click += new System.EventHandler(this.btnModifier_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 571);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.statusStrip1.Size = new System.Drawing.Size(709, 22);
            this.statusStrip1.TabIndex = 56;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(477, 17);
            this.toolStripStatusLabel1.Text = "Application développée par Simon CHAVAL dans le cadre d\'un exercice avec M. GARRI" +
    "DO";
            // 
            // Mail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1263, 593);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Mail";
            this.Text = "Mail";
            this.Load += new System.EventHandler(this.Mail_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.TextBox tbxSansAnnuaire;
        private System.Windows.Forms.Button btnAjouterAvecAnnuaire;
        private System.Windows.Forms.Button btnEnvoiMail;
        private System.Windows.Forms.Button btnAjouterSansAnnuaire;
        private System.Windows.Forms.Button btnAnnuaire;
        private System.Windows.Forms.Button btnParcourir;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbxAvecAnnuaire;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbxSansAnnuaire;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnQuitter;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbxAnnuaire;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbxMessage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbxPiecesJointes;
        private System.Windows.Forms.ComboBox cbxServeur;
        private System.Windows.Forms.TextBox tbxObjet;
        private System.Windows.Forms.TextBox tbxExpediteur;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnModifier;
        private System.Windows.Forms.Button btnAjouterPJ;
        private System.Windows.Forms.Button btnModifierPJ;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
    }
}