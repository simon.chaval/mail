﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Mail_Final
{
    public partial class Annuaire : Form
    {

        List<Personne> contacts;
        const byte NB_COL = 6;

        public Annuaire()
        {
            InitializeComponent();
            contacts = Outil.lectureContact();
        }
        //
        // Résumé :
        //    Au lancement de la fenêtre, créer le tableau des contacts
        //
        private void Annuaire_Load(object sender, EventArgs e)
        {
            dgvAnnuaire.ColumnCount = NB_COL;

            dgvAnnuaire.ColumnHeadersVisible = true;
            dgvAnnuaire.Columns[0].HeaderText = "Nom";
            dgvAnnuaire.Columns[1].HeaderText = "Prénom";
            dgvAnnuaire.Columns[2].HeaderText = "Adresse Mail";
            dgvAnnuaire.Columns[3].HeaderText = "Ville";
            dgvAnnuaire.Columns[4].HeaderText = "Code Postal";
            dgvAnnuaire.Columns[5].HeaderText = "Téléphone";

            dgvAnnuaire.RowHeadersVisible = false;
            formatageDataGridView();
            remplirDgv();
        }
        //
        // Résumé :
        //    Mettre à la bonne dimension la tableau
        //
        private void formatageDataGridView()
        {
            for (int i = 0; i < dgvAnnuaire.ColumnCount; i++)
            {
                dgvAnnuaire.ColumnHeadersDefaultCellStyle.Alignment =
                DataGridViewContentAlignment.MiddleCenter;
                dgvAnnuaire.Columns[i].SortMode =
                DataGridViewColumnSortMode.NotSortable;
                dgvAnnuaire.Columns[i].Width = dgvAnnuaire.Width / NB_COL;
                dgvAnnuaire.Columns[i].DefaultCellStyle.Alignment =
                DataGridViewContentAlignment.BottomRight;
            }
        }
        //
        // Résumé :
        //    Permet de remplir le tableau des contacts
        //
        private void remplirDgv()
        {
            dgvAnnuaire.Rows.Clear();

            foreach (Personne p in contacts)
            {
                int numeroLigne = dgvAnnuaire.Rows.Add();

                dgvAnnuaire[0, numeroLigne].Value = p.Nom;
                dgvAnnuaire[1, numeroLigne].Value = p.Prenom;
                dgvAnnuaire[2, numeroLigne].Value = p.getMail();
                dgvAnnuaire[3, numeroLigne].Value = p.Ville;
                dgvAnnuaire[4, numeroLigne].Value = p.CodePostal;
                dgvAnnuaire[5, numeroLigne].Value = p.Telephone;

            }
        }
        //
        // Résumé :
        //    Permet d'ouvrir la fenêtre permettant de créer un nouveau contact
        //
        private void btnAjouter_Click(object sender, EventArgs e)
        {
            AjouterContact fenetre = new AjouterContact();
            fenetre.ShowDialog();

            if (fenetre.DialogResult == DialogResult.OK)
            {
                contacts.Add(fenetre.Contact);
                remplirDgv();
                Outil.enregistrerContact(contacts);
                MessageBox.Show("Contact enregistré");
            }
        }
        //
        // Résumé :
        //    Permet la modification d'un contact existant dans l'annuaire
        //
        private void btnModifier_Click(object sender, EventArgs e)
        {
            if (dgvAnnuaire.SelectedRows.Count != 1)
            {
                MessageBox.Show("Merci de sélectionner qu'une ligne !");
            }
            else
            {
                Personne personne = contacts[dgvAnnuaire.SelectedRows[0].Index];
                ModifierContact fenetre = new ModifierContact();
                fenetre.Contact = personne;
                fenetre.ShowDialog();

                if (fenetre.DialogResult == DialogResult.OK)
                {
                    remplirDgv();
                    Outil.enregistrerContact(contacts);
                    MessageBox.Show("Contact modifié");
                }
            }
        }
        //
        // Résumé :
        //    Permet de supprimer un ou plusieurs contacts
        //
        private void btnSupprimer_Click(object sender, EventArgs e)
        {
            if (dgvAnnuaire.SelectedRows.Count == 0)
            {
                MessageBox.Show("Merci de sélectionnera au moins une ligne !");
                return;
            }
            DialogResult confirmation = MessageBox.Show("Etes-vous sur ?", "Supprimer des contacts", MessageBoxButtons.YesNo);
            if (confirmation == DialogResult.Yes)
            {
                foreach (DataGridViewRow r in dgvAnnuaire.SelectedRows)
                {
                    contacts.RemoveAt(r.Index);
                }

                remplirDgv();
                Outil.enregistrerContact(contacts);
            }
        }
        //
        // Résumé :
        //    Remise aux bonnes dimensions du tableau
        //
        private void dgvAnnuaire_SizeChanged(object sender, EventArgs e)
        {
            formatageDataGridView();
        }
        
    }
}
