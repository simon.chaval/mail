﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Net.Mail;

namespace Mail_Final
{
    public partial class Mail : Form
    {
        private MailMessage mail = new MailMessage();
        public Mail()
        {
            InitializeComponent();
        }


        //
        // Résumé :
        //    Permet d'envoyer un mail en fonction du serveur choisit
        //
        private void btnEnvoiMail_Click(object sender, EventArgs e)
        {
            SmtpClient clientSmtp = new SmtpClient(cbxServeur.Text);
            mail.Body = tbxMessage.Text;
            mail.Subject = tbxObjet.Text;

            if (tbxExpediteur.Text != "")
            {
                mail.From = new MailAddress(tbxExpediteur.Text);
            }

            try
            {
                clientSmtp.Send(mail);
                MessageBox.Show("Le message a été envoyé");
            }

            catch (Exception ex)
            {
                MessageBox.Show("Echec :" + ex.Message);
            }
        }
        //
        // Résumé
        //    Au lancement du programme, 
        private void Mail_Load(object sender, EventArgs e)
        {
            foreach (String nomServeur in Outil.listeServeur())
            {
                cbxServeur.Items.Add(nomServeur);
            }
            cbxAvecAnnuaire.SelectedItem = cbxAvecAnnuaire.Items[0];
            cbxSansAnnuaire.SelectedItem = cbxSansAnnuaire.Items[0];
            cbxAvecAnnuaire.DropDownStyle = ComboBoxStyle.DropDownList;
            cbxSansAnnuaire.DropDownStyle = ComboBoxStyle.DropDownList;
            cbxAnnuaire.DropDownStyle = ComboBoxStyle.DropDownList;

            cbxAnnuaire.DataSource = Outil.lectureContact();

            generationVisuel();
        }
        //
        // Résumé :
        //    Ferme l'application
        //
        private void btnQuitter_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //
        // Résumé :
        //    Ouvre la fenêtre de l'annuaire
        //
        private void btnAnnuaire_Click(object sender, EventArgs e)
        {
            if (new Annuaire().ShowDialog() == DialogResult.Cancel)
                cbxAnnuaire.DataSource = Outil.lectureContact();
        }
        //
        // Résumé :
        //    Vide les champs de l'application Mail
        //
        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            cbxServeur.Text = null;
            tbxSansAnnuaire.Text = null;
            tbxExpediteur.Text = null;
            tbxObjet.Text = null;
            tbxPiecesJointes.Text = null;
            tbxMessage.Text = null;

            mail = new MailMessage();
            generationVisuel();
        }
        //
        // Résumé :
        //    Permet d'ajouter différents destinataires en destinataires basiques ou copies ou copies cachées
        //
        private void btnAjouterSansAnnuaire_Click(object sender, EventArgs e)
        {
            try
            {
                MailAddress m = new MailAddress(tbxSansAnnuaire.Text);
                switch (cbxSansAnnuaire.SelectedItem.ToString())
                {
                    case "Destinataire":
                        mail.To.Add(m);
                        break;
                    case "Copie":
                        mail.CC.Add(m);
                        break;
                    case "Copie cachée":
                        mail.Bcc.Add(m);
                        break;
                }
                tbxSansAnnuaire.Text = null;
                generationVisuel();
            }
            catch
            {
                MessageBox.Show("Merci de vérifier l'adresse mail", "Message d'erreur", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }


        //
        // Résumé :
        //    Permet d'ajouter différents destinataires en destinataires basiques ou copies ou copies cachées via l'annuaire
        //
        private void btnAjouterAvecAnnuaire_Click(object sender, EventArgs e)
        {
            MailAddress m = ((Personne)cbxAnnuaire.SelectedValue).getMail();
            switch (cbxAvecAnnuaire.SelectedItem)
            {
                case "Destinataire":
                    mail.To.Add(m);
                    break;
                case "Copie":
                    mail.CC.Add(m);
                    break;
                case "Copie cachée":
                    mail.Bcc.Add(m);
                    break;
            }
            generationVisuel();
        }

        //
        // Résumé :
        //    Permet d'obtenir du code HTML donnant la liste des addreses mails
        //
        // Paramètres :
        //    nom :
        //          Chaine de caractère correspondant au nom
        //    mails :
        //          Collection des adresses mails
        //
        // Retourne :
        //    Chaine de caractère au format HTML de la list des addresses mails
        //
        private String genererListeMails(String nom, MailAddressCollection mails)
        {
            String ligne = "<strong>" + nom + " : </strong>";
            foreach (MailAddress m in mails)
            {
                ligne += m.Address + " , ";
            }
            ligne = ligne.Substring(0, ligne.Length - 2);
            ligne += "<br><br>";
            return ligne;
        }

        //
        // Résumé :
        //    Permet d'obtenir du code HTML donnant la liste des pièces jointes
        //
        // Paramètre :
        //    nom :
        //         Chaine de caractère correspondant au nom
        //    pieceJointe :
        //          Collection de pièces jointes
        //
        // Retourne :
        //    Chaine de caractère au format HTML de la liste des pièces jointes
        //
        private String genererPieceJointe(String nom, AttachmentCollection pieceJointe)
        {
            String ligne = "<strong>" + nom + " : </strong>";
            foreach (Attachment m in pieceJointe)
            {
                ligne += m.Name + " , ";
            }
            ligne = ligne.Substring(0, ligne.Length - 2);
            ligne += "<br><br>";
            return ligne;
        }
        //
        // Résumé :
        //    Permet d'avoir un visuel du mail avec les différents destinataires, l'objet et le message
        //
        private void generationVisuel()
        {
            String titre = "<h3><u>Visualiser votre mail</u></h3><br>";
            String destinataires = genererListeMails("Destinataires", mail.To);
            String copies = genererListeMails("Copies", mail.CC);
            String copiesCachees = genererListeMails("Copies cachées", mail.Bcc);
            String objet = "<strong>Objet : </strong>" + tbxObjet.Text + "<br>";
            String piecesJointes = genererPieceJointe("Pièces Jointes", mail.Attachments);
            String messageHtml = tbxMessage.Text.Replace("\n", "<br>");
            String message = "<hr><strong>Message : </strong><br>" + messageHtml + "<br>";



            webBrowser1.DocumentText = titre + destinataires + copies + copiesCachees + objet + piecesJointes + message;

        }
        //
        // Résumé :
        //    Permet d'accéder au fichier de l'ordinateur pour ajouter une pièce jointe
        //
        private void btnParcourir_Click(object sender, EventArgs e)
        {
            OpenFileDialog file = new OpenFileDialog();
            file.ShowDialog();
            tbxPiecesJointes.Text = file.FileName;
        }
        //
        // Résumé :
        //    Permet d'ouvrir la fenêtre de modification des destinataires des mails
        //
        private void btnModifier_Click(object sender, EventArgs e)
        {
            DialogResult r = new ModifierMailAdresse(mail).ShowDialog();
            if (r == DialogResult.Cancel) generationVisuel();
        }
        //
        // Résumé :
        //    Affiche le texte de l'objet et du message sur le visualiseur
        //
        private void tbxObjet_TextChanged(object sender, EventArgs e)
        {
            generationVisuel();
        }
        private void tbxMessage_TextChanged(object sender, EventArgs e)
        {
            generationVisuel();
        }
        //
        // Résumé
        //    Permet d'ajouter la pièce jointe sélectionnée au mail
        //
        private void btnAjouterPJ_Click(object sender, EventArgs e)
        {
            try
            {
                mail.Attachments.Add(new Attachment(tbxPiecesJointes.Text));
                tbxPiecesJointes.Clear();
                generationVisuel();
            }
            catch
            {
                MessageBox.Show("Merci d'ajouter une pièce jointe", "Message d'erreur", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        //
        // Résumé :
        //    Permet d'ouvrir la fenêtre de modification des pièces jointes ajoutées au mail
        //
        private void btnModifierPJ_Click(object sender, EventArgs e)
        {
            DialogResult r = new ModifierPJ(mail.Attachments).ShowDialog();
            if (r == DialogResult.OK) generationVisuel();
        }
    }
}

