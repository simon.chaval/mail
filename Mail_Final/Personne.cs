﻿using System;
using System.Net.Mail;


namespace Mail_Final
{
    public class Personne
    {

        private String nom;
        //
        // Résumé :
        //    Permet d'obtenir le nom
        //
        // Retourne :
        //    Le nom
        //
        // Exceptions :
        //    T:MissingFieldException
        //    value à la valeur null
        //
        public String Nom
        {
            get { return nom; }
            set
            {
                if (value == "")
                {
                    throw new MissingFieldException("Veuillez remplir le champs : Nom");
                }

                nom = value.ToUpper();
            }
        }
        //
        // Résumé :
        //    Permet d'obtenir le prenom
        //
        // Retourne :
        //    Le prenom
        //
        public String Prenom { get; set; }
        private MailAddress mail;
        //
        // Résumé :
        //    Permet d'obtenir l'adresse mail
        //
        // Retourne :
        //    L'adresse mail
        //
        public MailAddress getMail()
        {
            return mail;
        }
        //
        // Résumé :
        //    Permet de définir une adresse mail
        //
        // Paramètre :
        //    mailTexte :
        //          Chaine de caractère définissant une adresse mail
        //
        // Exceptions :
        //    T:MissingFieldException
        //    mailTexte à la valeur null
        //
        public void setMail(String mailTexte)
        {
            if (mailTexte == "")
            {
                throw new MissingFieldException("Veuillez saisir une adresse mail");
            }
            mail = new MailAddress(mailTexte);
        }
        private String ville;
        //
        // Résumé :
        //    Permet d'obtenir une ville
        //
        // Retourne :
        //    Une ville
        //
        public String Ville
        {
            get { return ville; }
            set
            {
                ville = value.ToUpper();
            }
        }

        private String codePostal;
        //
        // Résumé :
        //    Permet d'obtenir le code postal
        //
        // Exceptions :
        //    T:FormatException
        //    Le code postal doit être composé uniquement de chiffres + doit être composé de 5 chiffres
        //
        public String CodePostal
        {
            get { return codePostal; }
            set
            {
                if (value == "")
                {
                    codePostal = value;
                    return;
                }

                if (!int.TryParse(value, out int code))
                {
                    throw new FormatException("Le code postal doit être composé uniquement de chiffres");
                }

                if (value.Length != 5)
                {
                    throw new FormatException("Le code postal doit être de 5 chiffres");
                }
                codePostal = value;
            }
        }

        private String telephone;
        //
        // Résumé :
        //    Permet d'obtenir le telephone
        //
        // Excpetions :
        //    T:FormatException
        //    Le téléphone doit contenir que des chiffres + doit être composé de 10 chiffres
        //
        public String Telephone
        {
            get {
                return telephone;
            }
            set
            {
                if (value== "")
                {
                    telephone = value;
                    return;
                }

                if (!Int64.TryParse(value, out Int64 code))
                {
                    throw new FormatException("Le téléphone doit contenir que des chiffres");
                }

                if (value.Length != 10)
                {
                    throw new FormatException("Le numéro de téléphone doit être composé de 10 chiffres");
                }
                telephone = value;
            }
        }
        //
        // Résumé :
        //    Permet de convertir l'instance Personne au format Csv
        //
        // Retourne :
        //    Une chaine de caractère caractérisant un contact
        //
        public String convertiCsv()
        {
            return Nom + ";" + Prenom + ";" + mail.Address + ";" + Ville + ";" + CodePostal + ";" + Telephone;
        }

        //
        // Résumé :
        //    Permet de convertir l'instance en une chaine de caractère
        //
        // Retourne :
        //    Une chaine de caractère
        //
        public override string ToString()
        {
            return Nom + " " + Prenom + " (" + mail.Address + ")";
        }
    }
}
