﻿using System;
using System.Windows.Forms;

namespace Mail_Final
{
    public partial class ModifierContact : Form
    {
        public Personne Contact { get; set; }
        public ModifierContact()
        {
            InitializeComponent();
        }
        //
        // Résumé :
        //    Initialise les champs déjà remplis
        //
        private void ModifierContact_Load(object sender, EventArgs e)
        {
            tbxCodePostal.Text = Contact.CodePostal;
            tbxMail.Text = Contact.getMail().ToString();
            tbxNom.Text = Contact.Nom;
            tbxPrenom.Text = Contact.Prenom;
            tbxTel.Text = Contact.Telephone;
            tbxVille.Text = Contact.Ville;
            tbxCodePostal.Text = Contact.CodePostal;
        }
        //
        // Résumé :
        //    Confirme la modification du contact
        //
        private void btnValider_Click(object sender, EventArgs e)
        {
            Contact.Nom = tbxNom.Text;
            Contact.Prenom = tbxPrenom.Text;
            Contact.setMail(tbxMail.Text);
            Contact.Ville = tbxVille.Text;
            Contact.CodePostal = tbxCodePostal.Text;
            Contact.Telephone = tbxTel.Text;
        }
    }
}
