﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mail_Final
{
    public partial class ModifierMailAdresse : Form
    {

        private MailMessage mail;
        public ModifierMailAdresse(MailMessage mail)
        {
            InitializeComponent();
            this.mail = mail;
        }
        //
        // Résumé :
        //    Actualisation de la liste après la modification
        //
        private void ModifierMailAdresse_Load(object sender, EventArgs e)
        {
            chargerListes();
        }

        //
        // Résumé :
        //    Actualisation de la liste
        //
        private void chargerListes()
        {
            lstDestinataire.DataSource = null;
            lstDestinataire.DataSource = mail.To;
            lstCopie.DataSource = null;
            lstCopie.DataSource = mail.CC;
            lstCopieCachee.DataSource = null;
            lstCopieCachee.DataSource = mail.Bcc;
        }
        //
        // Résumé :
        //    Permet de supprimer un destinataire ajouter au mail que se soit en destinataire simple, copie ou copie cachée
        //
        private void btnSupprimer_Click(object sender, EventArgs e)
        {
            DialogResult confirmation = MessageBox.Show("Etes-vous sur de vouloir supprimer ?", "Supprimer", MessageBoxButtons.YesNo);
            if (confirmation == DialogResult.Yes)
            {
                foreach (MailAddress m in lstDestinataire.SelectedItems)
                {
                    mail.To.Remove(m);
                }
                foreach (MailAddress m in lstCopie.SelectedItems)
                {
                    mail.CC.Remove(m);
                }
                foreach (MailAddress m in lstCopieCachee.SelectedItems)
                {
                    mail.Bcc.Remove(m);
                }
            }
            chargerListes();
        }
    }
}
