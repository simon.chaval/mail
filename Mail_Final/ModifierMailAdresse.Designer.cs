﻿namespace Mail_Final
{
    partial class ModifierMailAdresse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstDestinataire = new System.Windows.Forms.ListBox();
            this.lstCopieCachee = new System.Windows.Forms.ListBox();
            this.lstCopie = new System.Windows.Forms.ListBox();
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.lblDestinataire = new System.Windows.Forms.Label();
            this.lblCopie = new System.Windows.Forms.Label();
            this.lblCopieCachee = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lstDestinataire
            // 
            this.lstDestinataire.FormattingEnabled = true;
            this.lstDestinataire.Location = new System.Drawing.Point(92, 70);
            this.lstDestinataire.Name = "lstDestinataire";
            this.lstDestinataire.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lstDestinataire.Size = new System.Drawing.Size(200, 186);
            this.lstDestinataire.TabIndex = 0;
            // 
            // lstCopieCachee
            // 
            this.lstCopieCachee.FormattingEnabled = true;
            this.lstCopieCachee.Location = new System.Drawing.Point(755, 70);
            this.lstCopieCachee.Name = "lstCopieCachee";
            this.lstCopieCachee.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lstCopieCachee.Size = new System.Drawing.Size(225, 186);
            this.lstCopieCachee.TabIndex = 1;
            // 
            // lstCopie
            // 
            this.lstCopie.FormattingEnabled = true;
            this.lstCopie.Location = new System.Drawing.Point(401, 70);
            this.lstCopie.Name = "lstCopie";
            this.lstCopie.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lstCopie.Size = new System.Drawing.Size(242, 186);
            this.lstCopie.TabIndex = 2;
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.Location = new System.Drawing.Point(423, 306);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(190, 47);
            this.btnSupprimer.TabIndex = 3;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.UseVisualStyleBackColor = true;
            this.btnSupprimer.Click += new System.EventHandler(this.btnSupprimer_Click);
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.Location = new System.Drawing.Point(737, 377);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Fermer";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // lblDestinataire
            // 
            this.lblDestinataire.AutoSize = true;
            this.lblDestinataire.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDestinataire.Location = new System.Drawing.Point(146, 35);
            this.lblDestinataire.Name = "lblDestinataire";
            this.lblDestinataire.Size = new System.Drawing.Size(81, 13);
            this.lblDestinataire.TabIndex = 5;
            this.lblDestinataire.Text = "Destinataires";
            // 
            // lblCopie
            // 
            this.lblCopie.AutoSize = true;
            this.lblCopie.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCopie.Location = new System.Drawing.Point(494, 35);
            this.lblCopie.Name = "lblCopie";
            this.lblCopie.Size = new System.Drawing.Size(45, 13);
            this.lblCopie.TabIndex = 6;
            this.lblCopie.Text = "Copies";
            // 
            // lblCopieCachee
            // 
            this.lblCopieCachee.AutoSize = true;
            this.lblCopieCachee.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCopieCachee.Location = new System.Drawing.Point(824, 35);
            this.lblCopieCachee.Name = "lblCopieCachee";
            this.lblCopieCachee.Size = new System.Drawing.Size(98, 13);
            this.lblCopieCachee.TabIndex = 7;
            this.lblCopieCachee.Text = "Copies Cachées";
            // 
            // ModifierMailAdresse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1047, 496);
            this.Controls.Add(this.lblCopieCachee);
            this.Controls.Add(this.lblCopie);
            this.Controls.Add(this.lblDestinataire);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnSupprimer);
            this.Controls.Add(this.lstCopie);
            this.Controls.Add(this.lstCopieCachee);
            this.Controls.Add(this.lstDestinataire);
            this.Name = "ModifierMailAdresse";
            this.Text = "Modifier Adresse Mail";
            this.Load += new System.EventHandler(this.ModifierMailAdresse_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstDestinataire;
        private System.Windows.Forms.ListBox lstCopieCachee;
        private System.Windows.Forms.ListBox lstCopie;
        private System.Windows.Forms.Button btnSupprimer;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblDestinataire;
        private System.Windows.Forms.Label lblCopie;
        private System.Windows.Forms.Label lblCopieCachee;
    }
}