﻿using System;
using System.Windows.Forms;

namespace Mail_Final
{
    public partial class AjouterContact : Form
    {
        public Personne Contact { get; set; }

        public AjouterContact()
        {
            InitializeComponent();
        }

        private void AjouterContact_Load(object sender, EventArgs e)
        {
            Contact = new Personne();
        }
        //
        // Résumé :
        //    Permet d'enregistrer un contact
        //
        private void btnValider_Click(object sender, EventArgs e)
        {
            try
            {
                Contact.Nom = tbxNom.Text;
                Contact.Prenom = tbxPrenom.Text;
                Contact.setMail(tbxMail.Text);
                Contact.Ville = tbxVille.Text;
                Contact.CodePostal = tbxCodePostal.Text;
                Contact.Telephone = tbxTel.Text;
            }

            catch (Exception ex)
            {
                if (ex is FormatException || ex is MissingFieldException)
                {
                    this.DialogResult = DialogResult.None;
                    MessageBox.Show(ex.Message, "Message d'erreur", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
    }
}
